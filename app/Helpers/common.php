<?php
/**
 * Created by PhpStorm.
 * User: sinh
 * Date: 12/13/18
 * Time: 8:52 PM
 */
use Jenssegers\Agent\Agent;


if (!function_exists('coverImagePath')) {
    /**
     * Remove character in string, only get number
     * @return string
     */
    function coverImagePath($path){
        $agent = new Agent();
        $replacement= $agent->isDesktop() ? '/desktop' : '/mobile';
        $newPath = substr_replace($path, $replacement, strripos($path, '/'), 0);
        return $newPath;
    }
}
