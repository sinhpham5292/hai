<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class MainController extends Controller
{
    public function index(Request $request){
        $value = $request->value;
        if(isset($request->place)){
            $place = $request->place;
            return view('user.hai', compact(['place', 'value']));
        }
        return view('user.hai');
    }
}
