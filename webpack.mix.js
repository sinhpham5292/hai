const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/admin/app.js', 'public/admin/js')
//     .sass('resources/sass/admin/app.scss', 'public/admin/css')
//
//     //user
//     .js('resources/js/user/demo1/app.js', 'public/user/demo1/js')
//     .sass('resources/sass/user/demo1/app.scss', 'public/user/demo1/css');

