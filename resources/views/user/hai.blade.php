<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name = "format-detection" content = "telephone=no">


    <title>Thiệp Hồng</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <!-- Place favicon.ico in the root directory -->
    <!-- animate.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/animate.css') }}">
    <!-- bootstrap.min.css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- magnific-popup.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/magnific-popup.css') }}">
    <!-- slicknav.min.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/slicknav.min.css') }}">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- owl.carousel.min.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/owl.carousel.min.css') }}">
    <!-- flaticon.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/flaticon.css') }}">
    <!-- swiper.min.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/swiper.min.css') }}">
    <!-- default-css.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/default-css.css') }}">
    <!-- style.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/style.css') }}">
    <!-- responsive.css -->
    <link rel="stylesheet" href="{{ asset('user/hai/css/responsive.css') }}">


</head>
<body id="home">
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- preloader area start -->
{{--<div class="preloader">--}}
    {{--<div class="hart-wrap">--}}
        {{--<div class="heart"></div>--}}
    {{--</div>--}}
{{--</div>--}}
<audio id="myAudio" autoplay="autoplay" loop="loop" >
    <source src="{{ asset('user/hai/audio/1.ogg') }}" type="audio/ogg">
    <source src="{{ asset('user/hai/audio/1.mp3') }}" type="audio/mpeg">
</audio>
<!-- prealoader area end -->
<!-- header-area start -->
{{--<header id="home" class="sticky-header">--}}
    {{--<div class="header-area header-area2">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="logo">--}}
                        {{--<h1 class="d-inline-block pull-left"><a href="index.html">H<span><i class="fa fa-heart" aria-hidden="true"></i></span>N</a></h1>--}}
                        {{--<h1 class="d-inline-block pull-right text-white font-dacing">Thiệp cưới</h1>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</header>--}}
<!-- header-area end -->
<!-- slider-area start -->
<div class="slider-area slider-style-2 slider-two parallax scontent_loaded">
    <div class="swiper-container swiper-container-horizontal swiper-container-wp8-horizontal">
        <div class="swiper-wrapper" id="wp_slider">
            @for ($i = 1; $i <= 13; $i++)
                <div class="swiper-slide"
                     style="background: url({{ coverImagePath(asset('user/hai/img/slider/' .$i.'.jpg')) }}) no-repeat center center / cover;">
                </div>
            @endfor
            <div id="spirit-header" class="spirit-header">
                <canvas id="spirit-canvas"></canvas>
            </div>
            {{--<div id="video-background" class="video-player mb_YTPlayer isMuted"--}}
                 {{--data-property="{--}}
                        {{--videoURL:'https://www.youtube.com/watch?v=YhDlxiy3_6M',--}}
                        {{--containment:'.video-player',--}}
                        {{--autoPlay:true,--}}
                        {{--mute:false,--}}
                        {{--startAt:0,--}}
                        {{--opacity:1,--}}
                        {{--loop:true,--}}
                        {{--vol:100,--}}
                       {{--}"--}}
                 {{--style="background: url({{ asset('user/hai/img/slider/1.jpg') }}) center center / cover rgba(0, 0, 0, 0.5);">--}}
                {{--<div class="mbYTP_wrapper" id="wrapper_video-background" >--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <!-- Add Arrows -->
    </div>
    <div class="heart-center ">
        <div class="slider-content sd-default-content  wow pulse" data-wow-delay="1s" data-wow-iteration="infinite"
            style="    background: url({{ coverImagePath(asset('user/hai/img/slider/1.png')) }}) no-repeat center center / cover;"
        >
            <div class="col-lg-12  wow font-dacing zoomIn text-center">
                <p class="title font-dacing">Thiệp Mời</p>
                <div class="row">
                    <div class="col-6 wow zoomIn" data-wow-duration="2s">
                        <span class="male">Thanh</span><br/><span class="male ">Hải</span>
                    </div>
                    <div class="col-6 wow zoomIn" data-wow-duration="2s">
                        <span class="female ">Ngọc</span><br/><span class="female ">Lan</span>
                    </div>
                </div>
                <p class="date " style="">@if(isset($place) && $place === 'thanh-hon') 26-12-2018 @else  29-12-2018 @endif</p>
            </div>
        </div>

    </div>
</div>
<div class="count-down-area count-down-area-sub"
     style="background: url({{ coverImagePath(asset('user/hai/img/counter/2.jpg')) }}) no-repeat center center / cover;">
    <section class="count-down-section section-padding parallax" data-speed="7">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <h2 class="big"><span>We Are Waiting For.....</span> The Big Day</h2>
                </div>
                <div class="col-12 col-md-8 ">
                    <div class="count-down-clock">
                        <div id="clock" @if(isset($place) && $place === 'thanh-hon') data-time="2018/12/26 11:00:00" @else  data-time="2018/12/24 11:00:00"  @endif></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </section>
</div>
<!-- end count-down-section -->
<!-- slider-area end -->
<!-- couple-area start -->
<div id="cuple" class="couple-area pt--100 pb--80">
    <div class="container">
        <div class="col-l2">
            <div class="section-title text-center">
                <h2>Happy Cuple</h2>
                @if(isset($value))
                    <h3  style="    position: relative;
                                    font-size: 40px;
                                    font-family: 'Dancing Script', cursive;
                                    margin-bottom: 50px;
                                    color: #649e93;
                                ">
                        Thân mời {{ $value == 'anh' ? 'Anh' : ( $value == 'chi' ? 'Chị' : ($value == 'em' ? "Em" : 'Bạn') ) }} đến dự buổi tiệc cùng với gia đình chúng tôi!
                    </h3>
                @endif
            </div>
        </div>
        <div class="couple-item">
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 couple-single">
                    <div class="couple-img">
                        <img src="{{ coverImagePath(asset('user/hai/img/couple/1.jpg')) }}" alt="">
                    </div>
                    <div class="couple-content  mt--30">
                        <h4>Thanh Hải</h4>
                        <p>Hi I am Hải. I am going to introduce myself.I am a professional graphic designer.</p>
                    </div>
                </div>
                <div class="col-lg-2 couple-single">
                    <div class="couple-shape">
                        <img src="{{ coverImagePath(asset('user/hai/img/couple/2.png')) }}" alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 couple-single md-0">
                    <div class="couple-img">
                        <img src="{{ coverImagePath(asset('user/hai/img/couple/2.jpg')) }}" alt="">
                    </div>
                    <div class="couple-content mt--30">
                        <h4>Ngọc Lan</h4>
                        <p>Hi I am Lan. I am going to introduce myself.I am a professional graphic designer. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="event" class="service-area ptb--100">
    <div class="container">
        <div class="col-l2">
            <div class="section-title text-center">
                <h2>Thời gian & địa điểm</h2>
            </div>
        </div>
        <div class="service-area-menu">
            <ul class="nav nav-tabs row">
                <li class="col @if(isset($place) && $place === 'thanh-hon') active @endif px-0">
                    <a data-toggle="tab" href="#Wedding" class="@if(isset($place) && $place === 'thanh-hon') show active @endif">Lễ thành hôn</a>
                </li>
                <li class="col px-0 @if(isset($place) && $place === 'vu-quy') active @endif">
                    <a data-toggle="tab" href="#menu1" class="@if(isset($place) && $place === 'vu-quy') show active @endif">Lễ vu quy</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="Wedding" class="tab-pane fade @if(isset($place) && $place === 'thanh-hon') show active @endif">
                    <div class="Ceremony-wrap">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="ceromony-img map">
                                    {{--<img src="{{ asset('user/hai/img/service/1.jpg') }}" alt="">--}}
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d958.4700902383471!2d108.17851582653977!3d16.07169698887943!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m0!4m3!3m2!1d16.0717039!2d108.1791258!5e0!3m2!1svi!2sus!4v1543734287817"
                                            width="100%" height="300" frameborder="0" style="border:0" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="ceromony-content">
                                    <h3>Wedding Ceremony</h3>
                                    <span>Sunday, 25 July 18, 9.00 AM-5.00 PM</span>
                                    <span>256 Apay Road,Califonia Bong, London</span>
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a  normal </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade @if(isset($place) && $place === 'vu-quy') show active @endif">
                    <div class="Ceremony-wrap">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="ceromony-content ceromony-content2">
                                    <h3>Wedding Party</h3>
                                    <span>Sunday, 25 July 18, 9.00 AM-5.00 PM</span>
                                    <span>256 Apay Road,Califonia Bong, London</span>
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="ceromony-img map">
                                    {{--<img src="{{ asset('user/hai/img/service/2.jpg') }}" alt="">--}}
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m20!1m8!1m3!1d15335.426511262827!2d108.1843872!3d16.072928049999998!3m2!1i1024!2i768!4f13.1!4m9!3e6!4m3!3m2!1d16.0714847!2d108.1884642!4m3!3m2!1d16.071402199999998!2d108.1881638!5e0!3m2!1svi!2s!4v1543733293816"
                                            width="100%" height="300" frameborder="0" style="border:0" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service-area end -->
<!-- gallery area start -->
<div id="gallery" class="gallery-area pt--100 pb--70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>Our Gellary</h2>
                </div>
            </div>
        </div>
        <div class="row grid">
            @for ($i = 1; $i < 24; $i++)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 grid-item Pre-Wedding">
                    <div class="gallery-single">
                        <img src="{{ coverImagePath(asset('user/hai/img/gallery/' .$i. '.jpg')) }}" alt="">
                        <div class="social-1st">
                            <ul>
                                <li><a href="{{ coverImagePath(asset('user/hai/img/gallery/' .$i. '.jpg')) }}" class="expand-img"><i class="fa fa-search"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>
<!-- gallery area end -->
<!-- .footer-area start -->
<div class="footer-area" style="background: url({{ coverImagePath(asset('user/hai/img/footer/1.jpg')) }}) no-repeat center center / cover;">
    <div class="container">
        <div class="footer-content">
            <div class="content-sub">
                <h2>Xin cám ơn </h2>
                <span>For Being With Us</span>
            </div>
        </div>
    </div>
</div>

<!-- all js here -->
<!-- modernizr-3.5.0.min.js -->
<script src="{{ asset('user/hai/js/vendor/modernizr-3.5.0.min.js') }}"></script>
<!-- jquery-2.2.4.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- imageloded.min.js -->
<script src="{{ asset('user/hai/js/imageloded.min.js') }}"></script>
<!-- jquery.slicknav.min.js -->
<script src="{{ asset('user/hai/js/jquery.slicknav.min.js') }}"></script>
<!-- isotop.min.js -->
<script src="{{ asset('user/hai/js/isotop.min.js') }}"></script>
<!-- easing-min.js -->
<script src="{{ asset('user/hai/js/easing-min.js') }}"></script>
<!-- owl-carousel.js -->
<script src="{{ asset('user/hai/js/owl-carousel.js') }}"></script>
<!-- magnific-popup.min.js -->
<script src="{{ asset('user/hai/js/jquery.magnific-popup.min.js') }}"></script>
<!-- jquery.mb.YTPlayer.src.js -->
<script src="{{ asset('user/hai/js/jquery.mb.YTPlayer.src.js') }}"></script>
<!-- swiper.min.js -->
<script src="{{ asset('user/hai/js/swiper.min.js') }}"></script>
<!-- countdown.js -->
<script src="{{ asset('user/hai/js/countdown.js') }}"></script>

<!-- countdown.js -->
<script src="{{ asset('user/hai/js/spirit.js') }}"></script>

<!-- jquery-ripples.js -->
<script src="{{ asset('user/hai/js/jquery-ripples.js') }}"></script>
<!-- bootstrap.min.js -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- jquery-sticky-menu.js -->
<script src="{{ asset('user/hai/js/jquery-sticky-menu.js') }}"></script>
<!-- jquery-validation.js -->
<script src="{{ asset('user/hai/js/validation.js') }}"></script>
<!-- main.js -->
<script src="{{ asset('user/hai/js/main.js') }}"></script>
<!-- wow.min.js -->
<script src="{{ asset('user/hai/js/wow.min.js') }}"></script>

{{--<script src="{{ asset('user/hai/js/app.js') }}"></script>--}}
<script>
    new WOW().init();
</script>
</body>

</html>
