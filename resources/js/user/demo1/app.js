
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');


(function ($, window, document, undefined) {
    var daterangepickerInit = {
        init: function(){

        }
    }

    // please modulize your functions so we can reuse/turn on & off easily
    $(document).ready(function () {
        daterangepickerInit.init();
    });

})(jQuery, window, document);

